#!/usr/bin/env python
# coding: utf-8
# @author: ngaht


import pandas as pd


result1 = pd.read_csv('./exp/15nn_withoutCO2.csv') #0.67
result2 = pd.read_csv('./exp/svr_withoutCO2.csv') #0.41


sample = pd.read_csv('../data/sample_submission.csv')
sample['Ca'] = (result1['Ca']*0.33 + result2['Ca']*0.59)/(0.33+0.59)
sample['P'] = (result1['P']*0.33 + result2['P']*0.59)/(0.33+0.59)
sample['pH'] = (result1['pH']*0.33 + result2['pH']*0.59)/(0.33+0.59)
sample['SOC'] = (result1['SOC']*0.33 + result2['SOC']*0.51)/(0.33+0.59)
sample['Sand'] = (result1['Sand']*0.33 + result2['Sand']*0.51)/(0.33+0.59)

sample.to_csv('./exp/15nn_svr.csv', index = False)
