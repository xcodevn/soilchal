#!/usr/bin/env python
# coding: utf-8
#
# author: Thong Nguyen
# Simple k-NN implementation

import pandas as pd
import numpy  as np
from sklearn import svm, cross_validation
from sklearn import preprocessing
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline

train_data = pd.read_csv('../data/training.csv')
test_data  = pd.read_csv('../data/sorted_test.csv')

labels = train_data[['Ca','P','pH','SOC','Sand']].values

train_data.drop(['Ca', 'P', 'pH', 'SOC', 'Sand', 'PIDN'], axis=1, inplace=True)
test_data.drop(['PIDN'], axis=1, inplace=True)

# convert depth labels to binary
lb = preprocessing.LabelBinarizer()
train_data.Depth = lb.fit_transform(train_data.Depth)
test_data.Depth = lb.transform(test_data.Depth)

xtrain, xtest = np.array(train_data)[:,:3578], np.array(test_data)[:,:3578]

pca = PCA(n_components=10)
pca.fit(xtrain)

sup_vec = svm.SVR(C=10000.0, verbose = 2)
pipe = Pipeline(steps = [('pca', pca), ('svm', sup_vec)])

preds = np.zeros((xtest.shape[0], 5))
for i in range(5):
    pipe.fit(xtrain, labels[:,i])
    preds[:,i] = pipe.predict(xtest).astype(float)

sample = pd.read_csv('../data/sample_submission.csv')
sample['Ca'] = preds[:,0]
sample['P'] = preds[:,1]
sample['pH'] = preds[:,2]
sample['SOC'] = preds[:,3]
sample['Sand'] = preds[:,4]

sample.to_csv('beating_benchmark.csv', index = False)
