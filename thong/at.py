# coding: utf-8
# @author: Abhishek Thakur
# Beating the benchmark in Kaggle AFSIS Challenge.

import random
import pandas as pd
import numpy as np
import sklearn
from sklearn import svm, cross_validation
#from sklearn.pipeline import Pipeline
from sklearn.neighbors import KNeighborsRegressor
#from sklearn import tree
#from sklearn import linear_model

from sklearn import preprocessing
lb = preprocessing.LabelBinarizer()


#from sklearn.feature_selection import SelectKBest
#from sklearn.feature_selection import chi2

neigh = KNeighborsRegressor(n_neighbors=1)
#clf = tree.DecisionTreeRegressor()
#clf = linear_model.BayesianRidge()



train = pd.read_csv('../data/training.csv')
test = pd.read_csv('../data/sorted_test.csv')
labels = train[['Ca','P','pH','SOC','Sand']].values

train.drop(['Ca', 'P', 'pH', 'SOC', 'Sand', 'PIDN',
    "m2379.76","m2377.83", "m2375.9", "m2373.97", "m2372.04", "m2370.11",
 "m2368.18", "m2366.26", "m2364.33", "m2362.4" , "m2360.47", "m2358.54",
 "m2356.61", "m2354.68", "m2352.76" ], axis=1, inplace=True)
test.drop('PIDN', axis=1, inplace=True)
test.drop([ "m2379.76","m2377.83", "m2375.9", "m2373.97", "m2372.04", "m2370.11",
 "m2368.18", "m2366.26", "m2364.33", "m2362.4" , "m2360.47", "m2358.54",
 "m2356.61", "m2354.68", "m2352.76" ], axis=1, inplace=True)

lb.fit(train.Depth)
train.Depth = lb.transform(train.Depth)
test.Depth = lb.transform(test.Depth)

xtrain, xtest = np.array(train), np.array(test)

#from sklearn.cross_decomposition import PLSRegression
#pls2 = PLSRegression(n_components=4)

# correlation

#import scipy
#ar = np.ndarray([len(xtrain[0])])
#for i in range( len(xtrain[0]) ):
#    (ar[i],tp) =scipy.stats.pearsonr(xtrain[:,i], labels[:,0])

import matplotlib.pyplot as plt


sup_vec = svm.SVR(kernel = 'poly', C=10000.0, verbose = 2)

from sklearn.svm import LinearSVC
from sklearn.lda import LDA
#clf = Pipeline([ ('feature_selection', LinearSVC(C=0.01, penalty="l1", dual=False)), ('classification', sup_vec) ])

preds = np.zeros((xtest.shape[0], 5))
err = 0.0

flag = True

for i in range(0,5):
    #X_train, X_test, y_train, y_test = cross_validation.train_test_split(
    #        xtrain, labels[:,i] , test_size=2.0/5, random_state=3 )
    X_train = xtrain
    y_train = labels[:,i]

    clf = svm.SVR(C=10000.0, verbose = 2)

    if i == 1:
        #pls2.fit(X_train, y_train)
        #y_pre = np.array(pls2.predict(X_test))
        #y_pre = y_pre.astype(float)

        newtrain = []
        newlabel = []
        newtrain1 = []
        newlabel1 = []

        for k in range( len ( X_train ) ):
            if y_train[k] > 4.0:
                newtrain.append(X_train[k])
                newlabel.append(y_train[k])
            else:
                newtrain1.append(X_train[k])
                newlabel1.append(y_train[k])

        clf = svm.SVR(C=10000.0, verbose = 2)
        clf.fit(np.array(newtrain), np.array(newlabel))
        #y_pre = clf.predict(X_test)

        clf1 = svm.SVR(C=10000.0, verbose = 2)
        clf1.fit(np.array(newtrain1), np.array(newlabel1))
        y_pre1 = clf1.predict(xtest)

        #y_train = np.array([int(x) for x in y_train])
        #X_train = np.array(X_train)

        for k in range(len(y_train)):
            if y_train[k] > 4:
                y_train[k] = int(y_train[k])
            else:
                y_train[k] = 0

        clf2 = LDA(n_components=4)
        clf2.fit(X_train,y_train)
        y_pre2 = clf2.predict(xtest)
        neigh.fit(X_train, y_train)
        y_pre3 = neigh.predict(xtest)
        #plt.plot(y_pre,c="red")
        #plt.plot(y_pre1, c="blue")
        #plt.plot(y_pre2, c="black")
        #plt.plot(y_test, c="green")
        plt.show()
        yy = []
        # for kk in range(len(y_pre1)):
        #     if y_pre2[kk] > 2 or y_pre3[kk] > 2:
        #         yy.append( (y_pre2[kk] + y_pre3[kk]) )
        #     else:
        #         yy.append(y_pre1[kk])

        # y_pre = np.array(yy)
        preds[:,i] = y_pre1 # y_pre.astype(float)
    else:
        clf.fit(X_train,y_train)
        #sup_vec.fit(X_train, y_train)
        #y_pre = clf.predict(xtest)
        #score = sklearn.metrics.mean_squared_error(y_test, y_pre)
        #err = err + score

        #print "Score of feature %d cross test is %f" % (i, score)

        preds[:,i] = clf.predict(xtest).astype(float)


print "Overall score is %f" % (err / 5)
sample = pd.read_csv('../data/sample_submission.csv')
sample['Ca'] = preds[:,0]
sample['P'] = preds[:,1]
sample['pH'] = preds[:,2]
sample['SOC'] = preds[:,3]
sample['Sand'] = preds[:,4]

sample.to_csv('beating_benchmark.csv', index = False)

