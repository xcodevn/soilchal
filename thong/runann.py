import pandas as pd
import numpy as np
import sklearn
from sklearn import svm, cross_validation

from sklearn import preprocessing
lb = preprocessing.LabelBinarizer()


train = pd.read_csv('../data/training.csv')
test = pd.read_csv('../data/sorted_test.csv')
labels = train[['Ca','P','pH','SOC','Sand']].values

train.drop(['Ca', 'P', 'pH', 'SOC', 'Sand', 'PIDN',
    "m2379.76","m2377.83", "m2375.9", "m2373.97", "m2372.04", "m2370.11",
 "m2368.18", "m2366.26", "m2364.33", "m2362.4" , "m2360.47", "m2358.54",
 "m2356.61", "m2354.68", "m2352.76" ], axis=1, inplace=True)
test.drop('PIDN', axis=1, inplace=True)
test.drop([ "m2379.76","m2377.83", "m2375.9", "m2373.97", "m2372.04", "m2370.11",
 "m2368.18", "m2366.26", "m2364.33", "m2362.4" , "m2360.47", "m2358.54",
 "m2356.61", "m2354.68", "m2352.76" ], axis=1, inplace=True)

lb.fit(train.Depth)
train.Depth = lb.transform(train.Depth)
test.Depth = lb.transform(test.Depth)

xtrain, xtest = np.array(train), np.array(test)

from fann2 import libfann

ann = libfann.neural_net()
ann.create_from_file("nn.net")

rl =  ann.run(xtrain)
print rl[0]
print labels[0]
d = rl - labels[:,[1]]
d = d*d
print (sum(d) / len(xtrain) )

# print "Overall score is %f" % (err / 5)
# sample = pd.read_csv('../data/sample_submission.csv')
# sample['Ca'] = preds[:,0]
# sample['P'] = preds[:,1]
# sample['pH'] = preds[:,2]
# sample['SOC'] = preds[:,3]
# sample['Sand'] = preds[:,4]
#
# sample.to_csv('beating_benchmark.csv', index = False)
#
