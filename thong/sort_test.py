import random
import pandas as pd
import numpy as np
import sklearn
from sklearn import svm, cross_validation
from sklearn.neighbors import KNeighborsRegressor
import matplotlib.pyplot as plt
from sklearn.cross_validation import KFold
from sklearn.cross_validation import StratifiedKFold
from sklearn.decomposition import PCA
pca = PCA(n_components=2)

from sklearn.lda import LDA

test = pd.read_csv('../data/sorted_test.csv')
preds = pd.read_csv('submission.csv')

xtest = test.values
gtest = pd.read_csv('groupings_test.csv')
group = {}
for i in range(len(gtest.Group)):
    group[int(100000*gtest.TMAP[i])] = gtest.Group[i]

print group

Gidx = [ [] for i in range(61) ]
x1 = [ [] for i in range(61) ] 
x2 = [ [] for i in range(61) ]
grcol = []
for i in range(len(test)):
    gr =  group[int(100000*test.TMAP[i])]
    grcol.append(gr)
    Gidx[gr-1].append(i)
    if test.Depth[i] == 0:
        x1[gr-1].append(xtest[i])
    else:
        x2[gr-1].append(xtest[i])

def concat(idx):
    rl = [] 
    for i in idx:
        rl = rl + Gidx[i]
    return rl


idxs = concat(range(38,61))
newtest = test.drop('PIDN', axis=1, inplace=False)
newtest.insert(len(newtest.columns), "Group", grcol)
#newtest.sort(['Group', 'REF1', 'REF2', 'REF3'], inplace=True)
atest = newtest.values[idxs]

def findOther(rowidx):
    gr= newtest.Group[rowidx]
    depth = - newtest.Depth[rowidx]
    lstn, ref1 = newtest.LSTN[rowidx], newtest.REF1[rowidx]
    rl = -1
    for i in range(newtest.shape[0]):
        if (newtest.Group[i], newtest.Depth[i], newtest.LSTN[i], newtest.REF1[i]) == (gr, depth, lstn, ref1):
                rl = i
                break
    return rl
    
    
pv = preds.pH.values
newpreds = preds.copy()

for i in range(len(preds)):
    o =  findOther(i)
    if o > -1:
        newpreds.Ca[i] = 0.8 * newpreds.Ca[i]  + 0.2*preds.Ca[o]
        newpreds.P[i] = 0.8 * newpreds.P[i]  + 0.2*preds.P[o]
        newpreds.pH[i] = 0.8 * newpreds.pH[i]  + 0.2*preds.pH[o]
        newpreds.SOC[i] = 0.8 * newpreds.SOC[i]  + 0.2*preds.SOC[o]
        newpreds.Sand[i] = 0.8 * newpreds.Sand[i]  + 0.2*preds.Sand[o]

newpreds.to_csv('submission1.csv', index = False)
