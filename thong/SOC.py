import random
import pandas as pd
import numpy as np
import sklearn
from sklearn import svm, cross_validation
import matplotlib.pyplot as plt
from sklearn.cross_validation import StratifiedKFold
from sklearn.cross_validation import KFold


from sklearn import preprocessing
lb = preprocessing.LabelBinarizer()

train = pd.read_csv('../data/training.csv')
test = pd.read_csv('../data/sorted_test.csv')

feature = 'SOC'

labels = train[[feature]].values

train.drop(['Ca', 'P', 'pH', 'SOC', 'Sand', 'PIDN',
 "m2379.76","m2377.83", "m2375.9", "m2373.97", "m2372.04", "m2370.11",
 "m2368.18", "m2366.26", "m2364.33", "m2362.4" , "m2360.47", "m2358.54",
 "m2356.61", "m2354.68", "m2352.76"
            ],
           axis=1, inplace=True)
test.drop('PIDN', axis=1, inplace=True)
test.drop([
 "m2379.76","m2377.83", "m2375.9", "m2373.97", "m2372.04", "m2370.11",
 "m2368.18", "m2366.26", "m2364.33", "m2362.4" , "m2360.47", "m2358.54",
 "m2356.61", "m2354.68", "m2352.76"
 ],
          axis=1, inplace=True)

lb.fit(train.Depth)
train.Depth = lb.transform(train.Depth)
test.Depth = lb.transform(test.Depth)

avgs1 = np.average(train[train.columns[range(0, 3578)]].values, axis=1)
avgs2 = np.average(test[train.columns[range(0, 3578)]].values, axis=1)

#train.drop(train.columns[range(0, 1001)], axis=1, inplace=True)
#test.drop(train.columns[range(0, 1001)], axis=1, inplace=True)          

xtrain, xtest = train.values, test.values

##for i in range(len(xtrain)):
##    xtrain[i] = xtrain[i] - avgs1[i]
##    pass
##
##for i in range(len(xtest)):
##    xtest[i] = xtest[i] - avgs2[i]               
##    pass


# correlation
import scipy
ar = np.ndarray([len(xtrain[0])])
for i in range( len(xtrain[0]) ):
    (ar[i],tp) =scipy.stats.pearsonr(xtrain[:,i], labels[:,0])

import matplotlib.pyplot as plt
plt.plot(ar)
plt.show()

from sklearn.svm import LinearSVC
from sklearn.lda import LDA
clf2 = LDA(n_components=4)

def MSE(a, b):
    c = a - b
    c = c * c
    return sum(c)/len(a)

#clf = Pipeline([ ('feature_selection', LinearSVC(C=0.01, penalty="l1", dual=False)), ('classification', sup_vec) ])

err = 0.0

#X_train, X_test, y_train, y_test = cross_validation.train_test_split(
#        xtrain, labels[:,i] , test_size=2.0/5, random_state=3 )
X_train = xtrain
y_train = labels[:,0]


##plt.hist(y_train)
##plt.show()
ytrain = y_train.copy()
##for i in range(len(y_train)):
##    if y_train[i] > 5:
##        y_train[i] = 5 # np.log(y_train[i]) # (y_train[i-1] + y_train[i+1])/2


##print(len(X_train[0]))
##
##for i in range(1000):
##     plt.plot(X_train[i])

##plt.show()

NFOLDS = 37
skf = KFold(len(X_train), n_folds=NFOLDS, shuffle=False)
sum_err = 0.0

reg = svm.SVR(gamma=0.00,
              epsilon=0.1,
              C=5000, verbose = 2,
              kernel='precomputed', probability=False)
from sklearn.metrics import mean_squared_error

def my_kernel(x, y):
    return np.dot(x, y.T)
count = 0

from sklearn.metrics import mean_squared_error
from sklearn.datasets import make_friedman1
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.metrics.pairwise import chi2_kernel

for trainidx, testidx in skf:
    count = count + 1
    if count < 0:
        continue
    xxtrain = X_train[trainidx]
    xxtest  = X_train[testidx]
    yytrain = y_train[trainidx]
    yytest  = ytrain[testidx]
    K = chi2_kernel(xxtrain, gamma=.5)


    #reg.fit(xxtrain,yytrain)
    reg.fit(K, yytrain)

    #pred = reg.predict(xxtest)
    
    pred = reg.predict(xxtest)
##    for kk in range(len(pred)):
##        if pred[kk] > 5:
##            pred[kk] = pred[kk]  + 3
##    plt.plot(yytest,c="red")
##    plt.plot(pred,c="blue")
##    plt.show()
    
    #pred1 = neigh.predict(xxtest)
    #pred = (pred + pred1) / 2
    err = mean_squared_error(pred, yytest)
    sum_err = sum_err + err

    print "\nError score of %s is %.5f" % (feature, err)

print "Overall error score if %s is %.5f" % (feature, sum_err / NFOLDS)

reg.fit(X_train, y_train)
preds = reg.predict(xtest)
print "err: %.5f" % mean_squared_error(reg.predict(X_train), ytrain)
sample = pd.read_csv('submission.csv')
sample[feature] = preds

sample.to_csv('submission.csv', index = False)

plt.hist(preds)
plt.show()



