# coding: utf-8
# @author: Thong Nguyen
# Neuron net!

import random
import pandas as pd
import numpy as np
import sklearn
from pybrain.tools.shortcuts import buildNetwork
from sklearn.decomposition import PCA
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.datasets import SupervisedDataSet
from sklearn.cross_validation import KFold
from sklearn.cross_validation import StratifiedKFold
import pybrain


from sklearn import preprocessing
lb = preprocessing.LabelBinarizer()

train = pd.read_csv('../data/training.csv')
test = pd.read_csv('../data/sorted_test.csv')
labels = train[['Ca','P','pH','SOC','Sand']].values

gtrain = pd.read_csv('groupings_train.csv')
group = {}
for i in range(len(gtrain.Group)):
    group[int(100000*gtrain.TMAP[i])] = gtrain.Group[i]


train.drop(['Ca', 'P', 'pH', 'SOC', 'Sand', 'PIDN',
    "m2379.76","m2377.83", "m2375.9", "m2373.97", "m2372.04", "m2370.11",
 "m2368.18", "m2366.26", "m2364.33", "m2362.4" , "m2360.47", "m2358.54",
 "m2356.61", "m2354.68", "m2352.76" ], axis=1, inplace=True)

test.drop('PIDN', axis=1, inplace=True)
test.drop([ "m2379.76","m2377.83", "m2375.9", "m2373.97", "m2372.04", "m2370.11",
 "m2368.18", "m2366.26", "m2364.33", "m2362.4" , "m2360.47", "m2358.54",
 "m2356.61", "m2354.68", "m2352.76" ], axis=1, inplace=True)

lb.fit(train.Depth)
train.Depth = lb.transform(train.Depth)
test.Depth = lb.transform(test.Depth)

train.drop(train.columns[range(0, 1000)], axis=1, inplace=True)
test.drop(train.columns[range(0, 1000)], axis=1, inplace=True)


#pca = PCA(n_components=3579)
#pca.fit(train)
newtrain = np.array(train) # pca.transform(train)
newtest  = np.array(test)  # pca.transform(test)

Gidx = [ [] for i in range(37) ]
for i in range(len(newtrain)):
    gr =  group[int(100000*train.TMAP[i])]
    Gidx[gr-1].append(i)

def concat(idx):
    rl = [] 
    for i in idx:
        rl = rl + Gidx[i]
    return rl




NFOLDS = 37
skf = KFold(37, n_folds=NFOLDS, shuffle=False)
sum_err = 0.0
from sklearn.metrics import mean_squared_error
net = buildNetwork(train.shape[1], 50, 10, 1 )

for trainidx, testidx in skf:
    print testidx
    ds = SupervisedDataSet(train.shape[1], 1)
    dt = SupervisedDataSet(train.shape[1], 0)
    trainer = BackpropTrainer(net, ds)
    trainer.verbose = True

    # adding sample
    for i in concat(trainidx):
        ds.addSample(newtrain[i], labels[i,1])

    trainer.trainUntilConvergence()

    # adding test
    for i in concat(testidx):
        dt.addSample(newtrain[i], labels[i,1])

    preds = net.activateOnDataset( dt )
    ytest = labels.values[concat(testidx)]
    err = mean_squared_error(preds, ytest)
    print "MSE: %.5f" % err


