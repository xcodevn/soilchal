# coding: utf-8
# @author: Abhishek Thakur
# Beating the benchmark in Kaggle AFSIS Challenge.

import random
import pandas as pd
import numpy as np
import sklearn
from sklearn import svm, cross_validation
#from sklearn.pipeline import Pipeline
from sklearn.neighbors import KNeighborsRegressor
#from sklearn import tree
#from sklearn import linear_model
import matplotlib.pyplot as plt
from sklearn.cross_validation import KFold
from sklearn.cross_validation import StratifiedKFold
from sklearn.decomposition import PCA
pca = PCA(n_components=2)

from sklearn.lda import LDA


from sklearn import preprocessing
lb = preprocessing.LabelBinarizer()


#from sklearn.feature_selection import SelectKBest
#from sklearn.feature_selection import chi2

neigh = KNeighborsRegressor(n_neighbors=30, weights = 'distance')
#clf = tree.DecisionTreeRegressor()
#clf = linear_model.BayesianRidge()

train = pd.read_csv('../data/training.csv')
test = pd.read_csv('../data/sorted_test.csv')
gtrain = pd.read_csv('groupings_train.csv')
group = {}
for i in range(len(gtrain.Group)):
    group[int(100000*gtrain.TMAP[i])] = gtrain.Group[i]

print group

feature = 'P'

labels = train[[feature]].values

train.drop(['Ca', 'P', 'pH', 'SOC', 'Sand', 'PIDN',
 "m2379.76","m2377.83", "m2375.9", "m2373.97", "m2372.04", "m2370.11",
 "m2368.18", "m2366.26", "m2364.33", "m2362.4" , "m2360.47", "m2358.54",
 "m2356.61", "m2354.68", "m2352.76"
            ],
           axis=1, inplace=True)
test.drop('PIDN', axis=1, inplace=True)
test.drop([
 "m2379.76","m2377.83", "m2375.9", "m2373.97", "m2372.04", "m2370.11",
 "m2368.18", "m2366.26", "m2364.33", "m2362.4" , "m2360.47", "m2358.54",
 "m2356.61", "m2354.68", "m2352.76"
 ],
          axis=1, inplace=True)

lb.fit(train.Depth)
train.Depth = lb.transform(train.Depth)
test.Depth = lb.transform(test.Depth)

avgs1 = np.average(train[train.columns[range(0, 1001)]].values, axis=1)
avgs2 = np.average(test[train.columns[range(0, 1001)]].values, axis=1)

##train = train.drop(train.columns[range(0, 1500)], axis=1, inplace=False)
##test = test.drop(test.columns[range(0, 1500)], axis=1, inplace=False)
##
##train = train.drop(train.columns[range(0, 400)], axis=1, inplace=False)
##test= test.drop(test.columns[range(0, 400)], axis=1, inplace=False)
##train = train.drop(train.columns[range(200, 700)], axis=1, inplace=False)
##test= test.drop(test.columns[range(200, 700)], axis=1, inplace=False)
##xxtrain = train.drop(train.columns[range(1175, 1194)], axis=1, inplace=False)
##xxtest= test.drop(test.columns[range(1175, 1194)], axis=1, inplace=False)

xtrain, xtest = train.values, test.values

for i in range(len(xtrain)):
    #xtrain[i] = xtrain[i] - avgs1[i]
    pass

for i in range(len(xtest)):
    #xtest[i] = xtest[i] - avgs2[i]
    pass

#from sklearn.feature_selection import VarianceThreshold
#sel = VarianceThreshold(threshold=(.99 * (1 - .99)))
#xtrain = sel.fit_transform(xtrain)
#xtest = sel.transform(xtest)


# drop feature 250-1000th

#from sklearn.cross_decomposition import PLSRegression
#pls2 = PLSRegression(n_components=4)

# correlation

#import scipy
#ar = np.ndarray([len(xtrain[0])])
#for i in range( len(xtrain[0]) ):
#    (ar[i],tp) =scipy.stats.pearsonr(xtrain[:,i], labels[:,0])

import matplotlib.pyplot as plt

from sklearn.svm import LinearSVC
from sklearn.lda import LDA

#clf = Pipeline([ ('feature_selection', LinearSVC(C=0.01, penalty="l1", dual=False)), ('classification', sup_vec) ])

err = 0.0

#X_train, X_test, y_train, y_test = cross_validation.train_test_split(
#        xtrain, labels[:,i] , test_size=2.0/5, random_state=3 )
X_train = xtrain
y_train = labels[:,0]
ytrain = y_train.copy()

##plt.hist(y_train)
##plt.show()
ytrain = y_train.copy()

ulti = y_train.copy()

##for i in range(len(ulti)):
##    if y_train[i] > 7:
##        ulti[i] = 1
##    else:
##        ulti[i] = -1


Gidx = [ [] for i in range(37) ]
y1 = [ [] for i in range(37) ]
y2 = [ [] for i in range(37) ]
x1 = [ [] for i in range(37) ]
x2 = [ [] for i in range(37) ]
for i in range(len(ytrain)):

    gr =  group[int(100000*train.TMAP[i])]
##    print gr
##    print Gidx
##    Gidx.append([])
    Gidx[gr-1].append(i)
    if train.Depth[i] == 0:
        y1[gr-1].append(ytrain[i])
        x1[gr-1].append(xtrain[i])
    else:
        y2[gr-1].append(ytrain[i])
        x2[gr-1].append(xtrain[i])

def getcolor(a):
    a = 1 + a
    return (1- a/15, 1 - a/15, 1- a/15)

##for i in range(37):
##    for j in range(len(x1[i])):
##        plt.plot(x1[i][j], c=getcolor(y1[i][j]), alpha=0.8)
##        plt.plot(y1[i])
##    for j in range(len(x2[i])):
##        plt.plot(y2[i])

##        plt.plot(x2[i][j], c=getcolor(y2[i][j]), alpha=0.8)
##    plt.show()

##ss = sklearn.preprocessing.StandardScaler(copy=True, with_std=True, with_mean=True)

#y_train = ss.fit_transform(y_train)
#plt.plot(y_train)
#plt.show()
for i in range(len(y_train)):
    if y_train[i] > 4:
        y_train[i] = 4 # np.log(y_train[i]) # (y_train[i-1] + y_train[i+1])/2

##print(len(X_train[0]))
##
##for i in range(1000):
##     plt.plot(X_train[i])

##plt.show()

NFOLDS = 37
skf = KFold(37, n_folds=NFOLDS, shuffle=False)
sum_err = 0.0

reg = svm.SVR(C=10000.0, verbose = 2, kernel='rbf', probability=False)
##clf = svm.SVC(C=5000.0)
clf2 = LDA(n_components=4)

def my_kernel(x, y):
    return np.dot(x, y.T)


from sklearn.metrics import mean_squared_error


def concat(idx):
    rl = []
    for i in idx:
        rl = rl + Gidx[i]
    return rl

for trainidx, testidx in skf:
    print testidx
    xxtrain = X_train[concat(trainidx)]
    xxtest  = X_train[concat(testidx)]
    yytrain = y_train[concat(trainidx)]
    yytest  = ytrain[concat(testidx)]
##    xulti = ulti[trainidx]

##    clf2.fit(xxtrain,yytrain)
##    pred = clf2.predict(xxtest)

    reg.fit(xxtrain,yytrain)
    neigh.fit(xxtrain, yytrain)
    pred1 = neigh.predict(xxtest)
    pred = reg.predict(xxtest)

    #clf2.fit(xxtrain,yict(xxtest)

    #p3 = clf2.predict_proba(xxtest)
##    plt.subplot(3, 1, 1)

##    plt.plot(yytest,c="red")
##    plt.plot(pred,c="green")
##    #plt.plot(y_pre2,c="green")
##    plt.plot(pred1,c="black")
##    plt.plot(cpred,c="blue")
##    plt.subplot(3, 1, 2)
##    for idx in testidx:
##        plt.plot(X_train[idx], alpha=0.8, c=getcolor(ytrain[idx]))

##    cor = pca.fit_transform(xxtest)
##    plt.subplot(3, 1, 3)
##    count = 0
##    for idx in testidx:
##        plt.scatter(cor[count,0], cor[count,1], c=getcolor(ytrain[idx]))
##        count = count + 1
##    plt.show()

    #pred = (pred + y_pre2)/2
    #for kk in range(len(pred)):
    #    if y_pre2[kk] > 2:
    #        pred[kk] = y_pre2[kk]

##    pred1 = neigh.predict(xxtest)
##    for k in range(len(pred)):
##        if cpred[k] >= 2:
##            pred[k] = cpred[k]
    #pred = (1*pred + 9*pred1) / 10
    err = mean_squared_error(pred, yytest)
    sum_err = sum_err + err

    print "\nError score of %s is %.5f" % (feature, err)

print "Overall error score if %s is %.5f" % (feature, sum_err / NFOLDS)

#clf3 = LDA(n_components=14)
neigh.fit(X_train, y_train)
reg.fit(X_train, y_train)
clf2.fit(X_train, y_train)
p2 = clf2.predict(xtest)
p3 = neigh.predict(xtest)
pred = reg.predict(xtest)
##plt.plot(pred, c= "red")

for kk in range(len(pred)):
    if p2[kk] > 5:
        pred[kk] = p2[kk]
## plt.plot(pred, c="blue")
##plt.plot(p3, c ="green")

##pred1 = neigh.predict(xtest)
##for k in range(len(pred1)):
##    if pred1[k] >= 2:
##        pred[k] = pred1[k]
##pred = (pred + pred1) / 2

print "err: %.5f" % mean_squared_error(reg.predict(X_train), ytrain)
sample = pd.read_csv('submission.csv')
sample[feature] = pred

sample.to_csv('submission.csv', index = False)

## plt.show()

